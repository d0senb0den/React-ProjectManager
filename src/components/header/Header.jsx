import React from 'react'
import './header.css'
import Logo from "../../assets/Logo.png"

const Header = () => {
  return (
    <div className='Title'>
        <img src={Logo} alt="" />
    </div>
  )
}

export default Header