// import Header from "./components/header/Header"
// import Content from "./components/content/Content"
// import Home from './components/home/Home'
// import Image from './components/image/Image'

// class App extends Component {
//     render(){
//         return (
//             <div>
//                 <BrowserRouter>
//                 <Link to="/home">HOME</Link>
//                 <Link to="/image">IMAGE UPLOAD</Link>
//                     <Routes>
//                         <Route exact path="/home" element={<Home/>}/>
//                         <Route exact path="/image" element={<Image/>}/>
//                     </Routes>
//                 <BrowserRouter/> 
//             <div/>
//         )
//     }
// }

// export default App

import React, { Component } from 'react';  
import { BrowserRouter as Router,Routes, Route, Link } from 'react-router-dom';  
import Home from './components/home/Home';
import Image from './components/image/Image';
   
class App extends Component {  
  render() {  
    return (  
       <Router>  
           <div className="App">  
            <ul className="App-header">  
              <li>  
                <Link to="/">Home</Link>  
              </li>  
              <li>  
                <Link to="/image">Upload</Link>  
              </li>
            </ul>  
           <Routes>  
                 <Route exact path='/' element={< Home />}></Route>  
                 <Route exact path='/image' element={< Image />}></Route> 
          </Routes>  
          </div>  
       </Router>  
   );  
  }  
}  
export default App;  