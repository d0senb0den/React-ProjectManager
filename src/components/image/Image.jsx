import { useState } from "react"

function Image() {
    const [image, setImage] = useState('')
    const handleChange = (e) => {
        setImage(e.target.files[0])
    }

    const handleApi = () => {
        //Call API

        // const url = ''

        // const formData = new FormData()
        // formData.append('image', image)
        // axios.post(url, formData).then((result) => {
        //     console.log(result.data)
        //     alert('Success')
        // })
        // .catch(error => {
        //     alert('Service error')
        //     console.log(error)
        // })
    }
    return(
        <div>
            Image Upload
            <input type="file" onChange={handleChange}/>
            <button onClick={handleApi}>Submit</button>
        </div>
    )
}

export default Image